package ru.geekbrains.lesson_4;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class TicTacToe {
    /**
     * Символ которым обозначается клетка занятая фишкой игрока бросившиго вызов компьютеру.
     */
    private static final char HUMAN_DOT = 'X';
    
    /**
     * Символ которым обозначается клетка занятая фишкой компьютера.
     */
    private static final char AI_DOT = 'O';
    
    /**
     * Символ которым обозначается никем не занятая игровая клетка.
     */
    private static final char EMPTY_DOT = '*';

    /**
     * @see Scanner
     */
    private static final Scanner sc = new Scanner(System.in);

    /**
     * @see Random
     */
    private static final Random rnd = new Random();

    /**
     * Высота игрового поля.
     */
    private static int mapSizeY;

    /**
     * Ширина игрового поля.
     */
    private static int mapSizeX;

    /**
     * Необходимое количество одинаковых фишек для победы, которые при этом должны следовать друг за другом
     * по вертикали, горизонтали или диагоняли.
     */
    private static int chipCount;

    /**
     * Игровое поле.
     */
    private static char[][] map;

    /**
     * Содержит список не занятых ячеек игрового поля.
     */
    private static ArrayList<ArrayList<Integer>> emptyCells = new ArrayList<ArrayList<Integer>>();

    /**
     * Задание 1.
     * Полностью разобраться с кодом, попробовать переписать с нуля, стараясь не подглядывать в методичку.
     *
     * @param args
     */
    public static void main(String[] args) {
        initMap(5, 5, 4);
        printMap();
        while (true) {
            humanTurn();
            printMap();
            if(checkWin(HUMAN_DOT)) {
                System.out.println("Выиграл игрок!");
                break;
            }
            if(isMapFull()) {
                System.out.println("Ничья!");
                break;
            }
            aiTurn();
            printMap();
            if(checkWin(AI_DOT)) {
                System.out.println("Выирал компьютер!");
                break;
            }
            if(isMapFull()) {
                System.out.println("Ничья!");
                break;
            }
        }
        sc.close();
    }

    /**
     * Инициализация пустого игрового поля.
     *
     * @param sizeX Количество ячеек игрового поля по горизонтали
     * @param sizeY Количество ячеек игрового поля по вертикали
     * @param chips Необходимое количество одинаковых фишек для победы, которые при этом должны следовать
     *              друг за другом по вертикали, горизонтали или диагоняли
     */
    private static void initMap(int sizeX, int sizeY, int chips) {
        mapSizeX = sizeX;
        mapSizeY = sizeY;
        chipCount = chips;
        map = new char[mapSizeY][mapSizeX];
        for (int y = 0; y < mapSizeY; y++) {
            for (int x = 0; x < mapSizeX; x++) {
                map[y][x] = EMPTY_DOT;
                ArrayList<Integer> emptyCell = new ArrayList<Integer>();
                emptyCell.add(0, x);
                emptyCell.add(1, y);
                emptyCells.add(emptyCell);
            }
        }
    }

    /**
     * Выводит игровое поле в консоли.
     */
    private static void printMap() {
        System.out.print("+ ");
        for (int i = 1; i <= mapSizeX; i++) System.out.print(i + " ");
        System.out.println();
        for (int i = 0; i < mapSizeY; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < mapSizeX; j++) System.out.print(map[i][j] + " ");
            System.out.println();
        }
        System.out.println();
    }

    /**
     * Обрабатывает ход игрока бросившиго вызов компьютеру.
     */
    private static void humanTurn() {
        int x, y;
        do {
            System.out.println("Введите координаты X и Y:");
            x = sc.nextInt() - 1;
            y = sc.nextInt() - 1;
        } while (!isValidCell(x, y) || !isEmptyCell(x, y));
        turn(x, y, HUMAN_DOT);
    }

    /**
     * Игровой ход компьютера.
     *
     * Задание 4.
     * Доработать искусственный интеллект, чтобы он мог блокировать ходы игрока.
     */
    private static void aiTurn() {
        ArrayList<Integer> cell;
        // проверяем наличие выигрышного хода, если такой имеется пользуемся случаем
        cell = getCellForWin(AI_DOT);
        if (!cell.isEmpty()) {
            turn(cell.get(0), cell.get(1), AI_DOT);
            return;
        }
        // блокируем ход игрока ведущий к победе если такие имеются
        cell = getCellForWin(HUMAN_DOT);
        if (!cell.isEmpty()) {
            turn(cell.get(0), cell.get(1), AI_DOT);
            return;
        }
        // в случае отсутствия выигрышных ходов и ходов противника требующих блокировки, то делаем ход на произвольную
        // пустую ячейку игрового поля
        int emptyCellIndex = rnd.nextInt(emptyCells.size());
        cell = emptyCells.get(emptyCellIndex);
        turn(cell.get(0), cell.get(1), AI_DOT);
    }

    /**
     * Игровой ход в следствии которого занимается ячейка игрового поля.
     *
     * @param x Координата ячейки игрового поля по оси X в которую совершается ход
     * @param y Координата ячейки игрового поля по оси Y в которую совершается ход
     * @param c Символ фишки которой совершается ход
     */
    private static void turn(int x, int y, char c) {
        map[y][x] = c;
        removeEmptyCell(x, y);
    }

    /**
     * Задание 2.
     * Переделать проверку победы, чтобы она не была реализована просто набором условий, например,
     * с использованием циклов.
     *
     * Задание 3.
     * Попробовать переписать логику проверки победы, чтобы она работала для поля 5х5 и количества фишек 4.
     * Очень желательно не делать это просто набором условий для каждой из возможных ситуаций.
     *
     * @param c Символ фишки для которой необходимо выполнить проверку на победу
     * @return True в случае если игрок с указанным символом фишки выиграл, в противном случае False
     */
    private static boolean checkWin(char c) {
        for (int y = 0; y < mapSizeY; y++) {
            for (int x = 0; x < mapSizeX; x++) {
                // проверка выигрыша по горизонтали
                if (checkWinByHorizontal(x, y, c)) return true;
                // проверка выигрыша по диагонали направленной вниз и вправо
                if (checkWinByRBDiagonal(x, y, c)) return true;
                // проверка выигрыша по вертикали
                if (checkWinByVertical(x, y, c)) return true;
                // проверка выигрыша по диагонали направленной вниз и влево
                if (checkWinByLBDiagonal(x, y, c)) return true;
            }
        }
        return false;
    }

    /**
     * Выполняет проверку выигрыша по горизонтали направленной вправо от заданной ячейки игрового поля.
     *
     * @param x Координата ячейки игрового поля по оси X
     * @param y Координата ячейки игрового поля по оси Y
     * @param c Символ фишки для которой необходимо выполнить проверку на победу
     * @return True в случае выигрыша, False если выигрыша нет
     */
    private static boolean checkWinByHorizontal(int x, int y, char c) {
        if (map[y][x] != c) return false;
        final int maxX = x + chipCount;
        if (maxX > mapSizeX) return false;
        for (int i = x; i < maxX; i++ ) {
            if (map[y][i] != c) return false;
        }
        return true;
    }

    /**
     * Выполняет проверку выигрыша по вертикали направленной вниз от заданной ячейки игрового поля.
     *
     * @param x Координата ячейки игрового поля по оси X
     * @param y Координата ячейки игрового поля по оси Y
     * @param c Символ фишки для которой необходимо выполнить проверку на победу
     * @return True в случае выигрыша, False если выигрыша нет
     */
    private static boolean checkWinByVertical(int x, int y, char c) {
        if (map[y][x] != c) return false;
        final int maxY = y + chipCount;
        if (maxY > mapSizeY) return false;
        for (int i = y; i < maxY; i++ ) {
            if (map[i][x] != c) return false;
        }
        return true;
    }

    /**
     * Выполняет проверку выигрыша по диагонали направленной от заданной точки в сторону нижнего правого
     * угла игрового поля.
     *
     * @param x Координата ячейки игрового поля по оси X
     * @param y Координата ячейки игрового поля по оси Y
     * @param c Символ фишки для которой необходимо выполнить проверку на победу
     * @return True в случае выигрыша, False если выигрыша нет
     */
    private static boolean checkWinByRBDiagonal(int x, int y, char c) {
        if (map[y][x] != c) return false;
        final int maxX = x + chipCount;
        final int maxY = y + chipCount;
        if (maxX > mapSizeX || maxY > mapSizeY) return false;
        for (int i = x, j = y; i < maxX && j < maxY; i++, j++ ) {
            if (map[j][i] != c) return false;
        }
        return true;
    }

    /**
     * Выполняет проверку выигрыша по диагонали направленной от заданной точки в сторону нижнего левого
     * угла игрового поля.
     *
     * @param x Координата ячейки игрового поля по оси X
     * @param y Координата ячейки игрового поля по оси Y
     * @param c Символ фишки для которой необходимо выполнить проверку на победу
     * @return True в случае выигрыша, False если выигрыша нет
     */
    private static boolean checkWinByLBDiagonal(int x, int y, char c) {
        if (map[y][x] != c) return false;
        final int minX = x - chipCount;
        final int maxY = y + chipCount;
        if (minX < -1 || maxY > mapSizeY) return false;
        for (int i = x, j = y; i > minX && j < maxY; i--, j++ ) {
            if (map[j][i] != c) return false;
        }
        return true;
    }

    /**
     * Проверяет является ли ячейка игрового поля пустой.
     *
     * @param x Координата ячейки игрового поля по оси X
     * @param y Координата ячейки игрового поля по оси Y
     * @return True если ячейка игрового поля является пустой, False если она уже занята
     */
    private static boolean isEmptyCell(int x, int y) {
        return map[y][x] == EMPTY_DOT;
    }

    /**
     * Проверяет находится ли точка в рамках игрового поля.
     *
     * @param x Координата ячейки игрового поля по оси X
     * @param y Координата ячейки игрового поля по оси Y
     * @return True если точка находится в пределах игрового поля, False если за пределами игрового поля
     */
    private static boolean isValidCell(int x, int y) {
        return x >= 0 && x < mapSizeX && y >= 0 && y < mapSizeY;
    }

    /**
     * Проверяет наличие свободных ячеек на игровом поле.
     *
     * @return True если свободных ячеек на игровом поле нет, в противном случае False
     */
    private static boolean isMapFull() { return emptyCells.isEmpty(); }

    /**
     * Удаляет координаты ячейки игрового поля из списка пустых ячеек.
     *
     * @param x Координата ячейки игрового поля по оси X
     * @param y Координата ячейки игрового поля по оси Y
     */
    private static void removeEmptyCell(int x, int y) {
        ArrayList<Integer> emptyCell = new ArrayList<Integer>();
        emptyCell.add(0, x);
        emptyCell.add(1, y);
        emptyCells.remove(emptyCell);
    }

    /**
     * Возвращает координаты пустой ячейки ход в которую приведет к выигрышу по горизонтали, вертикали или диагонали.
     *
     * @param c Символ фишки для которой необходимо получить координаты пустой ячейки ход в которую приведет к выигрышу
     * @return ArrayList<Integer> с координатами пуйстой ячейки в которую необходимо совершить ход для выигрыша,
     *         в случае отсутсвия ячейки ведущей к выигрышу объект будет пустой. В объекте элемент с индексом 0 содержит
     *         координаты по оси X, объект с индексом 1 содержит координаты по оси Y.
     */
    private static ArrayList<Integer> getCellForWin(char c) {
        ArrayList<Integer> cell = new ArrayList<Integer>();
        for (int y = 0; y < mapSizeY; y++) {
            for (int x = 0; x < mapSizeX; x++) {
                // поиск пустой ячейки ход в которую приведет к выигрышу по горизонтали
                cell = getCellForWinByHorizontal(x, y, c);
                if (!cell.isEmpty()) return cell;
                // поиск пустой ячейки ход в которую приведет к выигрышу по диагонали направленной вниз и вправо
                cell = getCellForWinByRBDiagonal(x, y, c);
                if (!cell.isEmpty()) return cell;
                // поиск пустой ячейки ход в которую приведет к выигрышу по вертикали
                cell = getCellForWinByVertical(x, y, c);
                if (!cell.isEmpty()) return cell;
                // поиск пустой ячейки ход в которую приведет к выигрышу по диагонали направленной вниз и влево
                cell = getCellForWinByLBDiagonal(x, y, c);
                if (!cell.isEmpty()) return cell;
            }
        }
        return cell;
    }

    /**
     * Возвращает координаты пустой ячейки ход в которую приведет выигрышу по горизонтали направленной вправо
     * относительно ячейки с переданными координатами.
     *
     * @param x Координата ячейки игрового поля по оси X относительно которой производится поиск выигрышной ячейки
     * @param y Координата ячейки игрового поля по оси Y относительно которой производится поиск выигрышной ячейки
     * @param c Символ фишки для которой необходимо получить координаты пустой ячейки ход в которую приведет к выигрышу
     * @return ArrayList<Integer> с координатами пуйстой ячейки в которую необходимо совершить ход для выигрыша,
     *         в случае отсутсвия ячейки ведущей к выигрышу объект будет пустой. В объекте элемент с индексом 0 содержит
     *         координаты по оси X, объект с индексом 1 содержит координаты по оси Y.
     */
    private static ArrayList<Integer> getCellForWinByHorizontal(int x, int y, char c) {
        if (!isEmptyCell(x, y) && map[y][x] != c) return new ArrayList<Integer>();
        final int maxX = x + chipCount;
        if (maxX > mapSizeX) return new ArrayList<Integer>();
        ArrayList<Integer> emptyCell = new ArrayList<Integer>();
        for (int i = x; i < maxX; i++ ) {
            if (!isEmptyCell(i, y) && map[y][i] != c) return new ArrayList<Integer>();
            if (isEmptyCell(i, y)) {
                if (emptyCell.size() != 0) return new ArrayList<Integer>();
                emptyCell.add(0, i);
                emptyCell.add(1, y);
            }
        }
        return emptyCell;
    }

    /**
     * Возвращает координаты пустой ячейки ход в которую приведет выигрышу по вертикали направленной вниз
     * относительно ячейки с переданными координатами.
     *
     * @param x Координата ячейки игрового поля по оси X относительно которой производится поиск выигрышной ячейки
     * @param y Координата ячейки игрового поля по оси Y относительно которой производится поиск выигрышной ячейки
     * @param c Символ фишки для которой необходимо получить координаты пустой ячейки ход в которую приведет к выигрышу
     * @return ArrayList<Integer> с координатами пуйстой ячейки в которую необходимо совершить ход для выигрыша,
     *         в случае отсутсвия ячейки ведущей к выигрышу объект будет пустой. В объекте элемент с индексом 0 содержит
     *         координаты по оси X, объект с индексом 1 содержит координаты по оси Y.
     */
    private static ArrayList<Integer> getCellForWinByVertical(int x, int y, char c) {
        if (!isEmptyCell(x, y) && map[y][x] != c) return new ArrayList<Integer>();
        final int maxY = y + chipCount;
        if (maxY > mapSizeY) return new ArrayList<Integer>();
        ArrayList<Integer> emptyCell = new ArrayList<Integer>();
        for (int i = y; i < maxY; i++ ) {
            if (!isEmptyCell(x, i) && map[i][x] != c) return new ArrayList<Integer>();
            if (isEmptyCell(x, i)) {
                if (emptyCell.size() != 0) return new ArrayList<Integer>();
                emptyCell.add(0, x);
                emptyCell.add(1, i);
            }
        }
        return emptyCell;
    }

    /**
     * Возвращает координаты пустой ячейки ход в которую приведет выигрышу по диагонали направленной вниз и вправо
     * относительно ячейки с переданными координатами.
     *
     * @param x Координата ячейки игрового поля по оси X относительно которой производится поиск выигрышной ячейки
     * @param y Координата ячейки игрового поля по оси Y относительно которой производится поиск выигрышной ячейки
     * @param c Символ фишки для которой необходимо получить координаты пустой ячейки ход в которую приведет к выигрышу
     * @return ArrayList<Integer> с координатами пуйстой ячейки в которую необходимо совершить ход для выигрыша,
     *         в случае отсутсвия ячейки ведущей к выигрышу объект будет пустой. В объекте элемент с индексом 0 содержит
     *         координаты по оси X, объект с индексом 1 содержит координаты по оси Y.
     */
    private static ArrayList<Integer> getCellForWinByRBDiagonal(int x, int y, char c) {
        if (!isEmptyCell(x, y) && map[y][x] != c) return new ArrayList<Integer>();
        final int maxX = x + chipCount;
        final int maxY = y + chipCount;
        if (maxX > mapSizeX || maxY > mapSizeY) return new ArrayList<Integer>();
        ArrayList<Integer> emptyCell = new ArrayList<Integer>();
        for (int i = x, j = y; i < maxX && j < maxY; i++, j++ ) {
            if (!isEmptyCell(i, j) && map[j][i] != c) return new ArrayList<Integer>();
            if (isEmptyCell(i, j)) {
                if (emptyCell.size() != 0) return new ArrayList<Integer>();
                emptyCell.add(0, i);
                emptyCell.add(1, j);
            }
        }
        return emptyCell;
    }

    /**
     * Возвращает координаты пустой ячейки ход в которую приведет выигрышу по диагонали направленной вниз и влево
     * относительно ячейки с переданными координатами.
     *
     * @param x Координата ячейки игрового поля по оси X относительно которой производится поиск выигрышной ячейки
     * @param y Координата ячейки игрового поля по оси Y относительно которой производится поиск выигрышной ячейки
     * @param c Символ фишки для которой необходимо получить координаты пустой ячейки ход в которую приведет к выигрышу
     * @return ArrayList<Integer> с координатами пуйстой ячейки в которую необходимо совершить ход для выигрыша,
     *         в случае отсутсвия ячейки ведущей к выигрышу объект будет пустой. В объекте элемент с индексом 0 содержит
     *         координаты по оси X, объект с индексом 1 содержит координаты по оси Y.
     */
    private static ArrayList<Integer> getCellForWinByLBDiagonal(int x, int y, char c) {
        if (!isEmptyCell(x, y) && map[y][x] != c) return new ArrayList<Integer>();
        final int minX = x - chipCount;
        final int maxY = y + chipCount;
        if (minX < -1 || maxY > mapSizeY) return new ArrayList<Integer>();
        ArrayList<Integer> emptyCell = new ArrayList<Integer>();
        for (int i = x, j = y; i > minX && j < maxY; i--, j++ ) {
            if (!isEmptyCell(i, j) && map[j][i] != c) return new ArrayList<Integer>();
            if (isEmptyCell(i, j)) {
                if (emptyCell.size() != 0) return new ArrayList<Integer>();
                emptyCell.add(0, i);
                emptyCell.add(1, j);
            }
        }
        return emptyCell;
    }
}
